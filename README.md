In the face of climate change and increasing strain on urban traffic,
cycling and walking are becoming increasingly popular modes of
transportation, especially in larger cities. Already today, many
people use their bike to commute to work. On these trips, the cyclists
are exposed to environmental stressors such as air pollution, noise emmisions and heat. In this project we
generate and propose alternative commuting routes to cyclists that aim
to reduce this exposure to the environmental factors. In a field study
we investigate if the cyclists are willing to change their commuting
behavior based on these recommendations.

Our routing model is based on data of traffic flow as well as noise
level in the city of Leipzig.

![traffic](./img/traffic.png "Traffic as used in our model.")
*Traffic*

![alternative](./img/alternative.png "Alternative route.")
*Alternative routes*
